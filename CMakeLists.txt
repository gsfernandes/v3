cmake_minimum_required(VERSION 3.0)
project(v3)

# build_type=debug if cloned from git
set(default_build_type "Release")
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  set(default_build_type "Debug")
endif()
if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
  set(BISON_FLAGS "--debug")
endif()

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})

set(SRCS "")

# Runs flex and bison
find_package(BISON)
find_package(FLEX)
bison_target(Parser src/parser.y ${CMAKE_CURRENT_BINARY_DIR}/parser.tab.c COMPILE_FLAGS "${BISON_FLAGS}")
flex_target(Lexer src/lexer.l ${CMAKE_CURRENT_BINARY_DIR}/lexer.c)
add_flex_bison_dependency(Lexer Parser)
list(APPEND SRCS ${BISON_Parser_OUTPUTS} ${FLEX_Lexer_OUTPUTS})

# ### TARGET ###
add_executable("${CMAKE_PROJECT_NAME}" ${SRCS})
