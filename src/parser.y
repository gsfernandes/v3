%{
  #include <stdio.h>
  #include <stdlib.h>

  // Declare stuff from Flex that Bison needs to know about:
  extern int yylex();
  extern int yyparse();
  extern FILE *yyin;
  void yyerror(const char *s);
%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  Initially (by default), yystype
// is merely a typedef of "int", but for non-trivial projects, tokens could
// be of any arbitrary data type.  So, to deal with that, the idea is to
// override yystype's default typedef to be a C union instead.  Unions can
// hold all of the types of tokens that Flex could return, and this this means
// we can return ints or floats or strings cleanly.  Bison implements this
// mechanism with the %union directive:
%union {
  int    ival  ;
  float  fval  ;
  char  *sval  ;
}

// Define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the %union:
%token <ival> INT
%token <fval> FLT
%token <sval> STR
%token <sval> OPR
%token <sval> KET
%token CBO "{"
%token CBC "}"
%token ENDL "\n"
%token ASGN "="
%type  <sval> prod code statement

%%
code:           %empty              { $$ = ""; }
        |       code statement endl { $$ = $2; printf("\n"); }
        ;

statement:      prod                { $$ = $1; }
        |       prod "=" prod       { $$ = $3; assign($1, $3); } // external function from unwritten C++ code
        ;

prod:           KET                 { $$ = $1; }
        |       OPR                 { $$ = $1; }
        |       prod KET            { $$ = dot($1, $2); } // external function from unwritten C++ code
        |       prod OPR            { $$ = dot($1, $2); } // external function from unwritten C++ code
        |       CBO code CBC        { $$ = $2; }
        |       CBO endl code CBC   { $$ = $3; }
        ;

endl:           ENDL
        |       endl ENDL
        ;

%%


int main(int argc, char** argv) {

  // Debug builds with tracing:
  #if YYDEBUG
    yydebug = 1;
  #endif

  // Open a file handle to a particular file:
  FILE *src = fopen(argv[1], "r");
  // Make sure it is valid:
  if (!src) {
    printf("Can't open file\n");
    return -1;
  }
  // Set Flex to read from it instead of defaulting to STDIN:
  yyin = src;
  
  // Parse through the input until eof:
  do {
      yyparse();
  } while (!feof(yyin));

  printf("\n");

}

void yyerror(const char *s) {
  printf("Parse error: %s\n", s);
  // might as well halt now:
  exit(-1);
}

