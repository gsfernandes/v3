%{
    #include "parser.tab.h"
    extern int yylex();
%}

%option noyywrap


digit                   [0-9]
ID                      [a-zA-Z0-9\-_]
IDwSpc                  [ ]?{ID}[ ]?

%%

[\t]                      ;

[\n]                      { return ENDL ;}

[\{]                      { return CBO ;}

[\}]                      { return CBC ;}

{digit}+\.{digit}+        { yylval.fval = atof(yytext); return FLT ;}

{digit}+                  { yylval.ival = atoi(yytext); return INT ;}

[=]                       { return ASGN ;}

[|]{IDwSpc}+[>]           {

    int i;
    for(i=1 ; i < strlen(yytext) && yytext[i] == ' ' ; i++){};
    memmove(
        &yytext[0],
        &yytext[i],
        strlen(yytext)
    );

    for(i=strlen(yytext) - 2 ; i > 0 && yytext[i] == ' ' ; i--){};
    memmove(
        &yytext[i+1],
        &yytext[strlen(yytext)],
        strlen(yytext)
    );
    yylval.sval = strdup(yytext);
    return KET ;

}

{ID}+                     { yylval.sval = strdup(yytext); return OPR ;}

[ ]                       ;

<<EOF>>                   { static int once = 0; return (once = !once) ? ENDL : 0; }

%%

